#include <iostream>

// Include SFML so we can use it in our game
#include <SFML/Window.hpp>

#include "UI.h"
#include "widgets/WindowWidget.h"
#include "FileTheme.h"

// Create an 800x600 SFML window for our game
sf::RenderWindow window(sf::VideoMode(800, 600), "BerserkUI Demo");

using namespace Berserk;

// The UI to use for the demo
UI gameUI;
FileTheme theme("resources/themes/demo/");

int main() {
	gameUI.setTheme(theme);

	gameUI.addWidget("testWidget", WidgetPtr(new WindowWidget()));
	gameUI.addWidget("testWidget2", WidgetPtr(new WindowWidget()));

	if (WindowWidgetPtr wid = gameUI.getWidget<WindowWidget>("testWidget")) {
		wid->setPosition(sf::Vector2i(50, 30));
	}

	// The main loop - keeps running until the window is closed
	while (window.isOpen()) {
		// Loop over every event raised since the last time
		sf::Event event;
		while (window.pollEvent(event)) {
			// Pass any events to the UI
			gameUI.handleEvent(event);

			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::A) {
					gameUI.bringToFront(gameUI.getWidget("testWidget"));
				}
				if (event.key.code == sf::Keyboard::B) {
					gameUI.bringToFront(gameUI.getWidget("testWidget2"));
				}
				if (event.key.code == sf::Keyboard::V) {
					gameUI.setVisible(!gameUI.isVisible());
				}
			}

			// User is trying to close the window
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}

		window.clear();

		// Render the UI
		gameUI.render(window);

		// Tell SFML to display updates to the window
		window.display();
	}
	return 0;
}
