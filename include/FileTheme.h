#pragma once

#include <unordered_map>

#include "Theme.h"

namespace Berserk {
	class FileTheme : public Theme {
	public:
		FileTheme(const std::string& path);

		const sf::Texture& getTexture(const std::string& widgetClassName);
		bool hasWidget(const std::string& widgetClassName);
	private:
		std::string path;
		std::unordered_map<std::string, sf::Texture> textures;
	};
}
