#pragma once

#include <memory>

#include <SFML/Graphics.hpp>

#include "Theme.h"

namespace Berserk {
	/**
	 * The base class for all widgets.
	 * @note A widget can be created and maintained without attaching it to a Berserk::UI.
	 *       The only purpose of Berserk::UI is to group related widgets into a coherent unit.
	 */
	class Widget {
	public:
		Widget();
		virtual ~Widget() = 0;

		/**
		 * Returns the size of the widget.
		 * @return A sf::Vector2u, where x is width & y is height.
		 */
		sf::Vector2u getSize();

		/**
		 * Sets the size of the widget.
		 * @note Subclasses should override this to enforce constraints!
		 * @param size The new size of the widget.
		 */
		void setSize(sf::Vector2u size);

		/**
		 * Returns the position of the widget, relative to the sf::RenderTarget.
		 * @return A sf::Vector2i containing the position.
		 */
		sf::Vector2i getPosition();

		/**
		 * Sets the position of the widget, relative to the sf::RenderTarget.
		 * @note Subclasses should override this to enforce constraints!
		 * @param position The new position of the widget.
		 */
		void setPosition(sf::Vector2i position);

		/**
		 * Returns whether the widget is globally visible.
		 * @note A widget may be flagged visible but not actually get rendered;
		 *       for example, if it is moved fully off of the screen, or is entirely
		 *       obscured by a larger, opaque widget.
		 * @return True if the widget is globally visible, else false.
		 */
		bool isVisible();

		/**
		 * Marks whether the widget is globally visible.
		 * @note A widget may be flagged visible but not actually get rendered;
		 *       for example, if it is moved fully off of the screen, or is entirely
		 *       obscured by a larger, opaque widget.
		 * @param visible Set to true to enable rendering of the widget, false to disable.
		 */
		void setVisible(bool visible);

		/**
		 * Renders the widget to the specified sf::RenderTarget.
		 * @param target The sf::RenderTarget to render to.
		 */
		virtual void render(sf::RenderTarget& target) = 0;

		/**
		 * Updates the widget according to the given timestep.
		 * @param timestep The time (in seconds) since the last update.
		 */
		virtual void update(double timestep) = 0;

		/**
		 * Handles a given sf::Event for the widget.
		 * @param event The sf::Event to process.
		 * @return True to signal non-propagation, false otherwise.
		 */
		virtual bool handleEvent(const sf::Event& event) = 0;

		/**
		 * Returns the name of the class.
		 * @return An std::string holding the name of the class.
		 */
		virtual std::string getClassName() = 0;

		void setTheme(Theme& theme);
		Theme* getTheme();
	private:
		Theme* theme;

		sf::Vector2i position;
		sf::Vector2u size;
		bool visible;
		bool active;
	};

	typedef std::shared_ptr<Berserk::Widget> WidgetPtr;
}
