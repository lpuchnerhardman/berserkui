#include "UI.h"

#include <iostream>

#include "Widget.h"

using namespace Berserk;

UI::UI() {
	visible = true;
    theme = nullptr;
}

WidgetPtr UI::addWidget(const std::string& name, WidgetPtr widget) {
	if (widgetsMap.count(name) > 0) {
		// Widget is already in UI, so skip
		return nullptr;
	}

	// Apply the theme, if it exists
    if (theme) {
    	widget->setTheme(*theme);
    }

	widgetsList.push_front(widget);
	widgetsMap.insert(std::pair<std::string, WidgetPtr>(name, widget));
	return widget;
}

void UI::bringToFront(WidgetPtr widget) {
	std::list<WidgetPtr>::iterator iter;
    for (iter = widgetsList.begin(); iter != widgetsList.end(); ++iter) {
		if (*iter == widget) {
			std::cout << widget->getClassName() << " brought to front." << std::endl;
			widgetsList.erase(iter);
			widgetsList.push_front(widget);
			return;
		}
    }
}

void UI::render(sf::RenderTarget& target) {
	if (!visible) {
		return;
	}

	// Render back-to-front
	std::list<WidgetPtr>::reverse_iterator iter;
    for (iter = widgetsList.rbegin(); iter != widgetsList.rend(); ++iter) {
		if ((*iter)->isVisible()) {
			(*iter)->render(target);
		}
	}
}

void UI::update(double timestep) {
	for (WidgetPtr iter : widgetsList) {
		iter->update(timestep);
	}
}

bool UI::handleEvent(const sf::Event& event) {
	for (WidgetPtr iter : widgetsList) {
		if (iter->handleEvent(event)) {
			// First widget to return true is always active
			bringToFront(iter);
			return true;
		}
	}
	return false;
}

void UI::setVisible(bool visible) {
	this->visible = visible;
}

bool UI::isVisible() {
	return visible;
}

void UI::setTheme(Theme& theme) {
	this->theme = &theme;

	// Apply the theme to all the widgets
    for (WidgetPtr iter : widgetsList) {
		iter->setTheme(*this->theme);
    }
}
