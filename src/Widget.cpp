#include "Widget.h"

using namespace Berserk;

Widget::Widget() {
	visible = true;
	active = false;
	position = sf::Vector2i(0, 0);
	size = sf::Vector2u(0, 0);
	theme = nullptr;
}
Widget::~Widget() { }

sf::Vector2u Widget::getSize() {
	return size;
}
void Widget::setSize(sf::Vector2u size) {
	this->size = size;
}

sf::Vector2i Widget::getPosition() {
	return position;
}
void Widget::setPosition(sf::Vector2i position) {
	this->position = position;
}

bool Widget::isVisible() {
	return visible;
}
void Widget::setVisible(bool visible) {
	this->visible = visible;
}

void Widget::setTheme(Theme& theme) {
	this->theme = &theme;
}
Theme* Widget::getTheme() {
	return this->theme;
}
