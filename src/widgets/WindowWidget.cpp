#include "widgets/WindowWidget.h"

#include "Util.h"

using namespace Berserk;

#define CORNER 8

WindowWidget::WindowWidget() : Widget() {
	setSize(sf::Vector2u(100, 60));
	setPosition(sf::Vector2i(10, 10));
	dragInitialized = false;
	dragging = false;
	draggable = true;
}

void WindowWidget::render(sf::RenderTarget& target) {
	sf::Vector2f size(getSize());
	sf::Vector2f position(getPosition());

	Theme* theme = getTheme();
	if (theme) {
		size -= sf::Vector2f(2 * CORNER, 2 * CORNER);

		const sf::Texture& tex = theme->getTexture(getClassName());

		// Size calculations
		sf::Vector2i cornerSize(CORNER, CORNER);
		sf::Vector2i textureSize(tex.getSize());
		sf::Vector2i backgroundSize(textureSize.x - 2 * CORNER, textureSize.y - 2 * CORNER);
		sf::Vector2i vBorderSize(CORNER, textureSize.y - 2 * CORNER);
		sf::Vector2i hBorderSize(textureSize.x - 2 * CORNER, CORNER);

		// Render background
		sf::Sprite bg(tex, sf::IntRect(cornerSize, backgroundSize));
		bg.setPosition(position + sf::Vector2f(cornerSize));
		bg.setScale(size.x / backgroundSize.x, size.y / backgroundSize.y);
		target.draw(bg);

		// Render borders
		sf::Sprite lBorder(tex, sf::IntRect(sf::Vector2i(0, CORNER), vBorderSize));
		lBorder.setPosition(position + sf::Vector2f(0.0f, CORNER));
		lBorder.setScale(1.0f, size.y / vBorderSize.y);
		target.draw(lBorder);

		sf::Sprite rBorder(tex, sf::IntRect(sf::Vector2i(textureSize.x - CORNER, CORNER), vBorderSize));
		rBorder.setPosition(position + sf::Vector2f(size.x + CORNER, CORNER));
		rBorder.setScale(1.0f, size.y / vBorderSize.y);
		target.draw(rBorder);

		sf::Sprite uBorder(tex, sf::IntRect(sf::Vector2i(CORNER, 0), hBorderSize));
		uBorder.setPosition(position + sf::Vector2f(CORNER, 0.0f));
		uBorder.setScale(size.x / hBorderSize.x, 1.0f);
		target.draw(uBorder);

		sf::Sprite bBorder(tex, sf::IntRect(sf::Vector2i(CORNER, textureSize.y - CORNER), hBorderSize));
		bBorder.setPosition(position + sf::Vector2f(CORNER, size.y + CORNER));
		bBorder.setScale(size.x / hBorderSize.x, 1.0f);
		target.draw(bBorder);

		// Render corners
		sf::Sprite ulCorner(tex, sf::IntRect(sf::Vector2i(0, 0), cornerSize));
		ulCorner.setPosition(position);
		target.draw(ulCorner);

		sf::Sprite urCorner(tex, sf::IntRect(sf::Vector2i(textureSize.x - CORNER, 0), cornerSize));
		urCorner.setPosition(position + sf::Vector2f(size.x + CORNER, 0.0f));
		target.draw(urCorner);

		sf::Sprite llCorner(tex, sf::IntRect(sf::Vector2i(0, textureSize.y - CORNER), cornerSize));
		llCorner.setPosition(position + sf::Vector2f(0.0f, size.y + CORNER));
		target.draw(llCorner);

		sf::Sprite lrCorner(tex, sf::IntRect(textureSize - cornerSize, cornerSize));
		lrCorner.setPosition(position + sf::Vector2f(size + sf::Vector2f(cornerSize)));
		target.draw(lrCorner);
	} else {
		// Fallback code for no theme
		sf::RectangleShape shape;
		shape.setSize(size);
		shape.setPosition(position);
		shape.setFillColor(sf::Color::Green);
		target.draw(shape);
	}
}

void WindowWidget::update(double timestep) { }

bool WindowWidget::handleEvent(const sf::Event& event) {
	if (event.type == sf::Event::MouseButtonPressed) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			return handleLeftMouseButtonPressed(event);
		}
	} else if (event.type == sf::Event::MouseButtonReleased) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			return handleLeftMouseButtonReleased(event);
		}
	} else if (event.type == sf::Event::MouseMoved) {
		return handleMouseMoved(event);
	}
	return false;
}

bool WindowWidget::handleLeftMouseButtonPressed(const sf::Event& event) {
	sf::Vector2i mouse = sf::Vector2i(event.mouseButton.x, event.mouseButton.y);
	sf::Vector2i position(getPosition());
	sf::Vector2i size(getSize());

	if (!Util::isPointInRect<int>(mouse, sf::IntRect(position, size))) {
		return false; // Window wasn't clicked, so propagate event
	}

	sf::Vector2i cornerSize(CORNER, CORNER);
	if (Util::isPointInRect<int>(mouse, sf::IntRect(position + cornerSize, size - 2 * cornerSize))) {
		dragInitialized = true;
		dragAnchor = sf::Vector2f();
		mousePos = sf::Vector2f(mouse);
	} else if (Util::isPointInRect<int>(mouse, sf::IntRect(position, cornerSize))) {
		// Upper-left corner
	} else if (Util::isPointInRect<int>(mouse, sf::IntRect(position + size - cornerSize, cornerSize))) {
		// Bottom-right corner
	} else if (Util::isPointInRect<int>(mouse, sf::IntRect(position + sf::Vector2i(size.x - CORNER, 0), cornerSize))) {
		// Upper-right corner
	} else if (Util::isPointInRect<int>(mouse, sf::IntRect(position + sf::Vector2i(0, size.y - CORNER), cornerSize))) {
		// Bottom-left corner
	}
	return true; // Window was clicked, so don't propagate event
}

bool WindowWidget::handleLeftMouseButtonReleased(const sf::Event& event) {
	if (dragging || dragInitialized) {
		dragging = false;
		dragInitialized = false;
		return true;
	}
	return false;
}

bool WindowWidget::handleMouseMoved(const sf::Event& event) {
	if (dragInitialized) {
		sf::Vector2f diff(event.mouseMove.x - mousePos.x, event.mouseMove.y - mousePos.y);
		mousePos += diff;
		dragAnchor += diff;
		if (dragAnchor.x * dragAnchor.x + dragAnchor.y * dragAnchor.y > 2000) {
			dragging = true;
			dragInitialized = false;
			setPosition(getPosition() + sf::Vector2i(dragAnchor));
		}
		return true;
	} else if (dragging) {
		sf::Vector2f diff(event.mouseMove.x - mousePos.x, event.mouseMove.y - mousePos.y);
		mousePos += diff;
		setPosition(getPosition() + sf::Vector2i(diff));
		return true;
	}
	return false;
}

std::string WindowWidget::getClassName() {
	return "WindowWidget";
}

void WindowWidget::setDraggable(bool draggable) {
	this->draggable = draggable;
}

bool WindowWidget::isDraggable() {
	return draggable;
}
